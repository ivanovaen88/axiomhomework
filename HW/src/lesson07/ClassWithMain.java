package lesson07;

import java.util.ArrayList;
import java.util.Arrays;

public class ClassWithMain {
    public static void main(String[] args) {

        MyArray<String> myArray = new MyArray();
        System.out.println(myArray.size());
        System.out.println(Arrays.toString(myArray.getTArray()));

        System.out.println("\nadd()");
        System.out.println("Array size is " + myArray.size());
        myArray.add("A");
        System.out.println(Arrays.toString(myArray.getTArray()));
        System.out.println("Array size is " + myArray.size());
        myArray.add("B");
        System.out.println(Arrays.toString(myArray.getTArray()));
        System.out.println("Array size is " + myArray.size());
        myArray.add("C");
        System.out.println(Arrays.toString(myArray.getTArray()));
        System.out.println("Array size is " + myArray.size());
        myArray.add("D");
        System.out.println(Arrays.toString(myArray.getTArray()));
        System.out.println("Array size is " + myArray.size());

        System.out.println("\nget()");
        System.out.println(myArray.get(3));
        System.out.println(myArray.get(6));

        System.out.println("\nremove() - index");
        System.out.println(Arrays.toString(myArray.getTArray()));
        myArray.remove(0);
        System.out.println(Arrays.toString(myArray.getTArray()));
        myArray.add("1");
        myArray.add("2");
        myArray.add("3");
        System.out.println(Arrays.toString(myArray.getTArray()));
        myArray.remove(5);
        System.out.println(Arrays.toString(myArray.getTArray()));
        myArray.remove(5);
        System.out.println(Arrays.toString(myArray.getTArray()));
        myArray.remove(6);
        System.out.println(Arrays.toString(myArray.getTArray()));
        myArray.remove(3);
        System.out.println(Arrays.toString(myArray.getTArray()));

        System.out.println("\nremove() - string");
        myArray.add("1");
        myArray.add("3");
        System.out.println(Arrays.toString(myArray.getTArray()));
        myArray.remove("3");
        System.out.println(Arrays.toString(myArray.getTArray()));
        myArray.remove("B");
        System.out.println(Arrays.toString(myArray.getTArray()));
        myArray.remove("2");
        System.out.println(Arrays.toString(myArray.getTArray()));
        myArray.remove("4");
        System.out.println(Arrays.toString(myArray.getTArray()));

        System.out.println("------------------------------------");
        MyArray<Integer> myArray1 = new MyArray();
        System.out.println(myArray1.size());
        System.out.println(Arrays.toString(myArray1.getTArray()));

        System.out.println("\nadd()");
        System.out.println("Array size is " + myArray1.size());
        myArray1.add(1);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        System.out.println("Array size is " + myArray1.size());
        myArray1.add(2);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        System.out.println("Array size is " + myArray1.size());
        myArray1.add(3);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        System.out.println("Array size is " + myArray1.size());
        myArray1.add(4);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        System.out.println("Array size is " + myArray1.size());

        System.out.println("\nget()");
        System.out.println(myArray1.get(3));
        System.out.println(myArray1.get(6));

        System.out.println("\nremove() - index");
        System.out.println(Arrays.toString(myArray1.getTArray()));
        myArray1.remove(0);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        myArray1.add(11);
        myArray1.add(22);
        myArray1.add(33);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        myArray1.remove(5);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        myArray1.remove(5);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        myArray1.remove(6);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        myArray1.remove(3);
        System.out.println(Arrays.toString(myArray1.getTArray()));

        System.out.println("\nremove() - element");
        myArray1.add(11);
        myArray1.add(22);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        myArray1.remove((Integer)2);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        myArray1.remove((Integer)3);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        myArray1.remove((Integer)2);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        myArray1.remove((Integer)4);
        System.out.println(Arrays.toString(myArray1.getTArray()));
        System.out.println("----------------");
        for (Integer elem: myArray1) {
            System.out.println(elem);
        }



    }
}

