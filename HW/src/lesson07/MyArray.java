package lesson07;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.Consumer;

class MyArray<T> implements Iterable<T>{
    private T[] tArray = (T[]) new Object[3];

    int size() {
        int numberOfElements = 0;
        for (int i = tArray.length - 1; i >= 0; i--) {
            if (tArray[i] != null) {
                numberOfElements = numberOfElements + 1;
            }
        }
        return numberOfElements;
    }

    void add(T elem) {
        for (int i = 0; i < tArray.length; i++) {
            if (tArray[i] == null) {
                tArray[i] = elem;
                return;
            }
        }
        T[] temp = (T[]) new Object[tArray.length * 2];
        for (int i = 0; i < tArray.length; i++) {
            temp[i] = tArray[i];
        }
        temp[tArray.length] = elem;
        tArray = temp;
    }

    T get(int index) {
        if (index < tArray.length) {
            return tArray[index];
        } else {
            try {
                throw new IllegalArgumentException("Out of range");
            } catch (IllegalArgumentException e) {
                System.out.println("Incorrect index");
            }
        }
        return null;
    }


    void remove(int index) {
        if (index >= tArray.length) {
            System.out.println("Index is out of the range!");
            return;
        }
        if (tArray[index] == null) {
            System.out.println("The array element is already empty.");
            return;
        }
        for (int i = 0; i < tArray.length - 1; i++) {
            if (i >= index) {
                tArray[i] = tArray[i + 1];
            }
        }
        tArray[tArray.length - 1] = null;
    }

    void remove(T t) {
        int index = tArray.length;
        for (int i = 0; i < tArray.length; i++) {
            if (t.equals(tArray[i])) {
                index = i;
                break;
            }
        }
        if (index != tArray.length) {
            for (int i = 0; i < tArray.length - 1; i++) {
                if (i >= index) {
                    tArray[i] = tArray[i + 1];
                }
            }
            tArray[tArray.length - 1] = null;
        } else {
            System.out.println("String is absent in the Array.");
        }
    }

    T[] getTArray() {
        return tArray;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyIterator<T>(tArray);
    }
}

class MyIterator<E> implements Iterator<E>{
    int indexPosition = 0;
    E[] internalArray;

    public MyIterator(E[] internalArray) {
        this.internalArray = internalArray;
    }

    @Override
    public boolean hasNext() {
        if(internalArray.length-1>=indexPosition){
            return true;
        }
        return false;
    }

    @Override
    public E next() {
        E value = internalArray[indexPosition];
        indexPosition++;
        return value;
    }

//    @Override
//    public void forEachRemaining(Consumer<? super E> action) {
//
//    }
//
//    @Override
//    public void remove() {
//
//    }
}
