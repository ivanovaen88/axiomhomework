package lesson03;

class MyArray {
    private String[] stringArray = new String[3];

    int size() {
        int numberOfElements = 0;
        for (int i = stringArray.length-1; i >=0 ; i--) {
            if (stringArray[i] != null) {
                numberOfElements = numberOfElements + 1;
            }
        }
        return numberOfElements;
    }

    void add(String str) {
        for (int i = 0; i < stringArray.length; i++) {
            if (stringArray[i] == null) {
                stringArray[i] = str;
                return;
            }
        }
        //initialArray = Arrays.copyOf(initialArray, 2 * size());
        // initialArray[size() / 2] = str;
        String[] temp = new String[stringArray.length * 2];
        for (int i = 0; i < stringArray.length; i++) {
            temp[i] = stringArray[i];
        }
        temp[stringArray.length] = str;
        stringArray = temp;
    }

    String get(int index) {
        if (index < stringArray.length) {
            return stringArray[index];
        } else {
            return "Incorrect index!";
        }
    }

    void remove(int index) {
        if (index >= stringArray.length) {
            System.out.println("Index is out of the range!");
            return;
        }
        if (stringArray[index] == null) {
            System.out.println("The array element is already empty.");
            return;
        }
        for (int i = 0; i < stringArray.length-1; i++) {
            if (i >= index) {
                stringArray[i] = stringArray[i + 1];
            }
        }
        stringArray[stringArray.length-1] = null;
    }

    void remove(String string) {
        int index = stringArray.length;
        for (int i = 0; i < stringArray.length; i++) {
            if (string.equals(stringArray[i])) {
                index = i;
                break;
            }
        }
        if (index != stringArray.length) {
            for (int i = 0; i < stringArray.length-1; i++) {
                if (i >= index) {
                    stringArray[i] = stringArray[i + 1];
                }
            }
            stringArray[stringArray.length-1] = null;
        } else {
            System.out.println("String is absent in the Array.");
        }
    }

    String[] getStringArray() {
        return stringArray;
    }
}