package lesson03;

import java.util.Arrays;

public class ClassWithMain {
    public static void main(String[] args) {

        MyArray myArray = new MyArray();
        System.out.println(myArray.size());
        System.out.println(Arrays.toString(myArray.getStringArray()));

        System.out.println("\nadd()");
        System.out.println("Array size is " + myArray.size());
        myArray.add("A");
        System.out.println(Arrays.toString(myArray.getStringArray()));
        System.out.println("Array size is " + myArray.size());
        myArray.add("B");
        System.out.println(Arrays.toString(myArray.getStringArray()));
        System.out.println("Array size is " + myArray.size());
        myArray.add("C");
        System.out.println(Arrays.toString(myArray.getStringArray()));
        System.out.println("Array size is " + myArray.size());
        myArray.add("D");
        System.out.println(Arrays.toString(myArray.getStringArray()));
        System.out.println("Array size is " + myArray.size());

        System.out.println("\nget()");
        System.out.println(myArray.get(3));
        System.out.println(myArray.get(6));

        System.out.println("\nremove() - index");
        System.out.println(Arrays.toString(myArray.getStringArray()));
        myArray.remove(0);
        System.out.println(Arrays.toString(myArray.getStringArray()));
        myArray.add("1");
        myArray.add("2");
        myArray.add("3");
        System.out.println(Arrays.toString(myArray.getStringArray()));
        myArray.remove(5);
        System.out.println(Arrays.toString(myArray.getStringArray()));
        myArray.remove(5);
        System.out.println(Arrays.toString(myArray.getStringArray()));
        myArray.remove(6);
        System.out.println(Arrays.toString(myArray.getStringArray()));
        myArray.remove(3);
        System.out.println(Arrays.toString(myArray.getStringArray()));

        System.out.println("\nremove() - string");
        myArray.add("1");
        myArray.add("3");
        System.out.println(Arrays.toString(myArray.getStringArray()));
        myArray.remove("3");
        System.out.println(Arrays.toString(myArray.getStringArray()));
        myArray.remove("B");
        System.out.println(Arrays.toString(myArray.getStringArray()));
        myArray.remove("2");
        System.out.println(Arrays.toString(myArray.getStringArray()));
        myArray.remove("4");
        System.out.println(Arrays.toString(myArray.getStringArray()));





    }
}