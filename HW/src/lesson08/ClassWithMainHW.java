package lesson08;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static java.lang.annotation.ElementType.METHOD;

class ClassWithNameHW {
    public static void main(String[] args) {
        ClassWithMethods classWithMethods = new ClassWithMethods();
        Method[] methods = classWithMethods.getClass().getDeclaredMethods();
        for (Method method: methods ) {
           Annotation[] annotations =  method.getDeclaredAnnotations();
            for (Annotation annotation: annotations) {
                if(annotation.annotationType().equals(MyAnnotationHW.class)){
                    try{
                        method.invoke(classWithMethods);
                    }  catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}

class ClassWithMethods{
    @Deprecated
    public void method1(){
        System.out.println("Method #1");
    }

    public void method2(){
        System.out.println("Method #2");
    }
    @MyAnnotationHW
    public void method3(){
        System.out.println("Method #3");
    }
}

@Retention(RetentionPolicy.RUNTIME)
@Target(METHOD)
@interface MyAnnotationHW {
}