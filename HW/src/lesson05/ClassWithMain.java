package lesson05;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ClassWithMain {
    public static void main(String[] args) {
        String[] shapeNames = {"Rectangle", "Circle", "Hexagon", "Trapezoid", "Triangle"};
        String[] calcOptions = {"Area", "Perimeter"};

        String chosenShapeName;
        int chosenCalcOptionNumber=0;

        System.out.println("Available shapes:");
        for (int i = 0; i < shapeNames.length; i++) {
            System.out.println((i + 1) + " - " + shapeNames[i]);
        }
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Please enter available shape name:");
            chosenShapeName = scanner.nextLine();
        } while (!isChosenOptionCorrect(chosenShapeName,shapeNames));

        System.out.println("Available calculation options:");
        for (int i = 0; i < calcOptions.length; i++) {
            System.out.println((i + 1) + " - " + calcOptions[i]);
        }

        boolean enteredValueStatus;

        System.out.println("Pleas choose available calculation option:");
        do {
            try {
                enteredValueStatus = true;
                Scanner sc = new Scanner(System.in);
                chosenCalcOptionNumber = sc.nextInt();
                if (chosenCalcOptionNumber > calcOptions.length || chosenCalcOptionNumber < 1) {
                    throw new IllegalArgumentException();
                }
            } catch (IllegalArgumentException | InputMismatchException ex) {
                System.out.println("Incorrect entered calc option. Pleas choose available calculation option:");
                enteredValueStatus = false;
            }
        } while (!enteredValueStatus);

        Shape chosenShape = null;
        switch (chosenShapeName) {
            case ("Rectangle"):
                chosenShape = new Rectangle();
                break;
            case ("Circle"):
                chosenShape = new Circle();
                break;
            case ("Hexagon"):
                chosenShape = new Hexagon();
                break;
            case ("Trapezoid"):
                chosenShape = new Trapezoid();
                break;
            case ("Triangle"):
                chosenShape = new Triangle();
                break;
            default:
                System.out.println("There are no such shape!");
                break;
        }

        if(chosenShape != null){
            switch (chosenCalcOptionNumber) {
                case (1):
                    System.out.println(chosenShape.getClass().getSimpleName() + " area is:");
                    System.out.println(chosenShape.calculateArea());
                    break;
                case (2):
                    System.out.println(chosenShape.getClass().getSimpleName() + " perimeter is:");
                    System.out.println(chosenShape.calculatePerimeter());
                    break;
                default:
                    System.out.println("Calc option incorrect");
            }
        }

    }


    private static boolean isChosenOptionCorrect (Object obj, Object[] objects) {
        boolean status = false;
        for (Object object : objects) {
            if (object.equals(obj)) {
                status = true;
                break;
            }
        }
        return status;
    }
}
