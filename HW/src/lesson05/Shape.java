package lesson05;

import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class Shape {

    public abstract double calculateArea();
    public abstract double calculatePerimeter();

    public Double inputValue() {
        boolean enteredValueStatus;
        double side = 0;
        do {
            try {
                enteredValueStatus = true;
                Scanner sc = new Scanner(System.in);
                side = sc.nextDouble();
                checkEnteredValue(side);
            } catch (IllegalArgumentException | InputMismatchException ex) {
                System.out.println("Entered value is incorrect value. Please enter number > 0:");
                enteredValueStatus = false;
            }
        } while (!enteredValueStatus);
        return side;
    }

    public void checkEnteredValue(Double d) {
        if (d<=Double.MIN_VALUE) {
            throw new IllegalArgumentException();
        }
    }
}