package lesson05;

public class Hexagon extends Shape {
    private double side;

    Hexagon() {
        System.out.println("Please enter hexagon side:");
        this.side = super.inputValue();
    }

    @Override
    public double calculateArea() {
        return 3 * Math.sqrt(3*side*side/2);
    }

    @Override
    public double calculatePerimeter() {
        return side *6;
    }
}