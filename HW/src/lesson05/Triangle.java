package lesson05;

public class Triangle extends Shape{
    private double side1, side2, side3;

    Triangle() {
        System.out.println("Please enter 1 side:");
        this.side1 = super.inputValue();
        System.out.println("Please enter 2 side:");
        this.side2 = super.inputValue();
        System.out.println("Please enter 3 side:");
        this.side3 = super.inputValue();
    }

    @Override
    public double calculateArea() {
        double p = calculatePerimeter() / 2;
        return Math.sqrt(p * (p - side1) * (p - side2) * (p - side3));
    }

    @Override
    public double calculatePerimeter() {
        return side1 + side2 + side3;
    }
}
