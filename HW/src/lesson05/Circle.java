package lesson05;

public class Circle extends Shape{
    private double radios;

    Circle() {
        System.out.println("Please enter circle radios:");
        this.radios = super.inputValue();
    }

    @Override
    public double calculateArea() {
        return Math.PI* radios * radios;
    }

    @Override
    public double calculatePerimeter() {
        return Math.PI* radios * 2;
    }
}