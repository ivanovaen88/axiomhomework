package lesson05;

public class Rectangle extends Shape {
    private double side1, side2;

    Rectangle(){
        System.out.println("Please enter weight:");
        this.side1 = super.inputValue();
        System.out.println("Please enter height:");
        this.side2 = super.inputValue();
    }

    @Override
    public double calculateArea() {
        return side1 * side2;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * (side1 + side2);
    }
}