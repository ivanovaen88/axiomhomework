package lesson05;

public class Trapezoid extends Shape{
    private double side1, side2, base1, base2;

    Trapezoid() {
        System.out.println("Please enter left side > 0:");
        this.side1 = super.inputValue();

        System.out.println("Please enter right side > 0:");
        this.side2 = super.inputValue();

        System.out.println("Please enter top base > 0:");
        this.base1 = super.inputValue();

        System.out.println("Please enter bottom base > 0:");
        this.base2 = super.inputValue();
    }

    @Override
    public double calculateArea() {
        return (base1 + base2)/2 * Math.sqrt(
                Math.pow(side1,2)-
                        Math.pow(
                                (Math.pow((base2-base1),2) + Math.pow(side1,2) - Math.pow(side2,2))/
                                        2 * (base2 - base1)
                                ,2)
        );
    }

    @Override
    public double calculatePerimeter() {
        return side1 + side2 + base1 + base2;
    }
}
