package lesson04;

import java.util.Scanner;

public class ClassWithMain {

    public static void main(String[] args) {
        String[] shapeNames = {"Rectangle", "Circle", "Hexagon", "Trapezoid", "Triangle"};
        String[] calcOptions = {"Area", "Perimeter"};

        String chosenShapeName;
        int chosenCalcOptionNumber;

        System.out.println("Available shapes:");
        for (int i = 0; i < shapeNames.length; i++) {
            System.out.println((i + 1) + " - " + shapeNames[i]);
        }
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Please enter available shape name:");
            chosenShapeName = scanner.nextLine();
        } while (!isChosenOptionCorrect(chosenShapeName,shapeNames));

        System.out.println("Available calculation options:");
        for (int i = 0; i < calcOptions.length; i++) {
            System.out.println((i + 1) + " - " + calcOptions[i]);
        }

        do {
            System.out.println("Pleas choose available calculation option:");
            chosenCalcOptionNumber = scanner.nextInt();
        } while (chosenCalcOptionNumber > calcOptions.length || chosenCalcOptionNumber < 1);

        Shape chosenShape;
        switch (chosenShapeName) {
            case ("Rectangle"):
                chosenShape = new Rectangle();
                break;
            case ("Circle"):
                chosenShape = new Circle();
                break;
            case ("Hexagon"):
                chosenShape = new Hexagon();
                break;
            case ("Trapezoid"):
                chosenShape = new Trapezoid();
                break;
            case ("Triangle"):
                chosenShape = new Triangle();
                break;
            default:
                chosenShape = new Shape("Something's going wrong!");
                break;
        }
        switch (chosenCalcOptionNumber) {
            case (1):
                System.out.println(chosenShape.getClass().getSimpleName() + " area is:");
                System.out.println(chosenShape.calculateArea());
                break;
            case (2):
                System.out.println(chosenShape.getClass().getSimpleName() + " perimeter is:");
                System.out.println(chosenShape.calculatePerimeter());
                break;
        }

    }


    private static boolean isChosenOptionCorrect (Object obj, Object[] objects) {
        boolean status = false;
        for (int i = 0; i < objects.length; i++) {
            if (objects[i].equals(obj)){
                status = true;
                break;
            }
        }
        return status;
    }
}
