package lesson04;

import java.util.Scanner;

public class Rectangle extends Shape {
    private double side1, side2;

    public void setSide1(double side1) {
        this.side1 = side1;
    }

    public void setSide2(double side2) {
        this.side2 = side2;
    }

    Rectangle(){
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Please enter width > 0:");
            setSide1(sc.nextDouble());
        } while (side1<Double.MIN_VALUE);
        do {
            System.out.println("Please enter height > 0:");
            setSide2(sc.nextDouble());
        } while (side2<Double.MIN_VALUE);
    }

    @Override
    public double calculateArea() {
        return side1 * side2;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * (side1 + side2);
    }
}