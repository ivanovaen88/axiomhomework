package lesson04;

import java.util.Scanner;

public class Circle extends Shape{
    private double radios;

    public void setRadios(double radios) {
        this.radios = radios;
    }

    Circle() {
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Please enter circle radios > 0:");
            setRadios(sc.nextDouble());
        } while (radios < Double.MIN_VALUE);
    }

    @Override
    public double calculateArea() {
        return Math.PI* radios * radios;
    }

    @Override
    public double calculatePerimeter() {
        return Math.PI* radios * 2;
    }
}
