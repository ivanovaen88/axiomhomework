package lesson04;

import java.util.Scanner;

public class Hexagon extends Shape {
    private double side;

    public void setSide(double side) {
        this.side = side;
    }

    Hexagon() {
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Please enter hexagon side > 0:");
            setSide(sc.nextDouble());
        } while (side < Double.MIN_VALUE);
    }

    @Override
    public double calculateArea() {
        return 3 * Math.sqrt(3*side*side/2);
    }

    @Override
    public double calculatePerimeter() {
        return side *6;
    }
}
