package lesson04;

import java.util.Scanner;

public class Trapezoid extends Shape{
    private double side1, side2, base1, base2;

    public void setSide1(double side1) {
        this.side1 = side1;
    }

    public void setSide2(double side2) {
        this.side2 = side2;
    }

    public void setBase1(double base1) {
        this.base1 = base1;
    }

    public void setBase2(double base2) {
        this.base2 = base2;
    }

    public Trapezoid() {
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Please enter left side > 0:");
            setSide1(sc.nextDouble());
        } while (side1 < Double.MIN_VALUE);
        do {
            System.out.println("Please enter right side > 0:");
            setSide2(sc.nextDouble());
        } while (side2 < Double.MIN_VALUE);
        do {
            System.out.println("Please enter top base: > 0");
            setBase1(sc.nextDouble());
        } while (base1 < Double.MIN_VALUE);
        do {
            System.out.println("Please enter bottom base: > 0");
            setBase2(sc.nextDouble());
        } while (base2 < Double.MIN_VALUE);
    }

    @Override
    public double calculateArea() {
        return (base1 + base2)/2 * Math.sqrt(
                Math.pow(side1,2)-
                        Math.pow(
                                (Math.pow((base2-base1),2) + Math.pow(side1,2) - Math.pow(side2,2))/
                                        2 * (base2 - base1)
                                ,2)
        );
    }

    @Override
    public double calculatePerimeter() {
        return side1 + side2 + base1 + base2;
    }
}
