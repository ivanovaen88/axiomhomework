package lesson04;

import java.util.Scanner;

public class Triangle extends Shape{
    private double side1, side2, side3;

    public void setSide1(double side1) {
        this.side1 = side1;
    }

    public void setSide2(double side2) {
        this.side2 = side2;
    }

    public void setSide3(double side3) {
        this.side3 = side3;
    }

    public Triangle() {
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Please enter 1 side > 0:");
            setSide1(sc.nextDouble());
        } while (side1 < Double.MIN_VALUE);
        do {
            System.out.println("Please enter 2 side > 0:");
            setSide2(sc.nextDouble());
        } while (side2 < Double.MIN_VALUE);
        do {
            System.out.println("Please enter 3 side > 0:");
            setSide3(sc.nextDouble());
        } while (side3 < Double.MIN_VALUE);
    }

    @Override
    public double calculateArea() {
        double p = calculatePerimeter() / 2;
        return Math.sqrt(p * (p - side1) * (p - side2) * (p - side3));
    }

    @Override
    public double calculatePerimeter() {
        return side1 + side2 + side3;
    }
}
